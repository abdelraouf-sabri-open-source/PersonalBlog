var destination = 'public';
var assetsDestination = 'assets';

var gulp = require('gulp'),
    plumber = require('gulp-plumber'),
    browserSync = require('browser-sync'),
    runSequence = require('run-sequence'),
    stylus = require('gulp-stylus'),
    uglify = require('gulp-uglify'),
    concat = require('gulp-concat'),
    jeet = require('jeet'),
    rupture = require('rupture'),
    koutoSwiss = require('kouto-swiss'),
    prefixer = require('autoprefixer-stylus'),
    imagemin = require('gulp-imagemin'),
    cp = require('child_process');
runSequence.options.ignoreUndefinedTasks = true;
var messages = {
    jekyllBuild: '<span style="color: grey">Running:</span> $ jekyll build'
};

/**
 * Build the Jekyll Site
 */
gulp.task('jekyll-build', function(done) {
    browserSync.notify(messages.jekyllBuild);
    return cp.spawn('jekyll', ['build']).on('close', done);
});

/**
 * Rebuild Jekyll & do page reload
 */
gulp.task('jekyll-rebuild', ['jekyll-build'], function() {
    browserSync.notify(messages.jekyllBuild);
    browserSync.reload();
});

/**
 * Wait for jekyll-build, then launch the Server
 */
gulp.task('browser-sync', function() {
    browserSync({
        server: {
            baseDir: destination
        }
    });
});

/**
 * Stylus task
 */
gulp.task('stylus', function() {
    gulp.src('src/styl/main.styl').pipe(plumber()).pipe(stylus({
        use: [koutoSwiss(), prefixer(), jeet(), rupture()],
        compress: true,
        'include css': true
    }))
    .pipe(gulp.dest(destination + '/assets/css'))
    .pipe(browserSync.reload({stream:true}))
    .pipe(gulp.dest(assetsDestination + '/css'))
});

/**
 * Fonts Task
 */
gulp.task('fonts', function() {
    return gulp.src('src/fonts/**/*')
    .pipe(gulp.dest(destination + '/assets/fonts/'))
    .pipe(browserSync.reload({stream:true}))
    .pipe(gulp.dest(assetsDestination + '/fonts'))
});

/**
 * Javascript Task
 */
gulp.task('js', function() {
    return gulp.src('src/js/**/*.js').pipe(plumber()).pipe(concat('main.js')).pipe(uglify())
    .pipe(gulp.dest(destination + '/assets/js/'))
    .pipe(browserSync.reload({stream:true}))
    .pipe(gulp.dest(assetsDestination + '/js'))
});

/**
 * Imagemin Task
 */
gulp.task('imagemin', function() {
    return gulp.src('src/img/**/*.{jpg,png,gif,ico}').pipe(plumber()).pipe(imagemin({
        optimizationLevel: 3,
        progressive: true,
        interlaced: true
    })).pipe(gulp.dest(destination + '/assets/img'))
    .pipe(browserSync.reload({stream:true}))
    .pipe(gulp.dest(assetsDestination + '/img'))
});

/**
 * Watch stylus files for changes & recompile
 * Watch html/md files, run jekyll & reload BrowserSync
 */
gulp.task('watch', function() {
    gulp.watch('src/styl/**/*.styl', ['stylus']);
    gulp.watch('src/js/**/*.js', ['js']);
    gulp.watch('src/fonts/**/*', ['fonts']);
    gulp.watch('src/img/**/*.{jpg,png,gif}', ['imagemin']);
    gulp.watch(['*.html', '_includes/*.html', '_layouts/*.html', 'all posts/**/*', '_config.yml'], ['jekyll-rebuild']);
});


gulp.task('parent-task', ['fonts' ,'js', 'stylus', 'imagemin']);

/**
 * Default task, running just `gulp` will compile the sass,
 * compile the jekyll site, launch BrowserSync & watch files.
 */
gulp.task('default', function () {
    runSequence('parent-task', 'jekyll-build');
});

gulp.task('dev', function () {
    runSequence('parent-task', 'jekyll-build', 'browser-sync','watch');
});