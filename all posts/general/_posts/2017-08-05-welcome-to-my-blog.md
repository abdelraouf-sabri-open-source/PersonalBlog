---
layout: post
title: "Welcome to my BLOG"
date: "2017-08-05 13:56:28"
image: '/assets/img/posts/thumbnails/2017-08-05-welcome-to-my-blog.jpg'
description: 'this is how and where I welcome my visitors'
tags: 
- First days
twitter_text: "Welcome to my BLOG"
permalink: /articles/:title/
block: "2x2"
type: post
---

![Welcome all of you](/assets/img/posts/thumbnails/2017-08-05-welcome-to-my-blog.jpg "Welcome all of You")

# Welcome my friends
After 30+ hours working with `Javascript`, `jekyll`, `gulp`, `gitlab-ci` and many others, I finally come to you in one piece. 

![One Piece](/assets/img/posts/One-Piece-Luffy-PNG-File.png "One Piece")

But not `Luffy`, it's `AbdElraouf`. Really it was a great journey, I learned a lot, most of the time fixing the mistakes I've done.

## How I'm changing the world ?
By god's will soon, I'm going to post a very good content about **Software Engineering** in general. Strating all over from the basics as if you know nothing. And helping make things better, if you already familiar with the content.

## What's expected from **YOU**
No thing, really no thing. But if you want to __support me__ that would be better, just leave a comment and let me know about you.

But the exercises I'm posting would __require you to be__ :
- concentrated
- write the code and see for yourself

I think that's very enough, to get started with the material I'm posting.

## Conclusion
This whole thing, is for free, no fees was nor would be required to read the content and learn something new. It's for god's sake. If only He, Allah, would be satisfied with me.

__See you__