#!/usr/bin/env bash

# ------------------------------------------------------------------------------
#
# Program: initpost.sh
# Author:  Vitor Britto
# Modified by:  James Bowling and AbdElraouf Sabri
# Author:  Vitor Britto (edited by Will) (edited by AbdElraouf Sabri)
# Description: script to create an initial structure for my posts.
#
# Usage: ./initpost.sh [options] <post title> <post image path> <type> <size> <category>
# Options:
#   -h, --help        output instructions
#   -c, --create      create post
#   -d, --draft       create draft post
#   -p, --publish     publish/promote a draft to a post
# Arguments:
#   <post title> : title of the post with quotes if it has spaces
#   <post image path> : absolute local image path
#   <type> : choose from {post, project, moment}
#   <size> : choose from {1x1, 1x2, 2x2}
#   <category> : post category
# Example:
#   ./initpost.sh -c "How to replace strings with sed" "/home/user/new-post.png" "post" "2x2" "category 1"
# Important Notes:
#   - Don't forget the quotes
#   - This script was created to generate new text files to my blog.
# Copyright (c) Vitor Britto
# Licensed under the MIT license.
# ------------------------------------------------------------------------------


# ------------------------------------------------------------------------------
# | VARIABLES                                                                  |
# ------------------------------------------------------------------------------

POST_TITLE="${2}"
SRC_THUMB_IMAGE="${3}"
POST_TYPE="${4}"
POST_SIZE="${5}"
POST_CATEGORY="${6}"

POST_NAME="$(echo ${2} | sed -e 's/ /-/g' | sed "y/ABCDEFGHIJKLMNOPQRSTUVWXYZ/abcdefghijklmnopqrstuvwxyz/")"
CURRENT_DATE="$(date +'%Y-%m-%d')"
TIME=$(date +"%T")
# ----------------------------------------------------------------


# SETTINGS: your configuration goes here
# ----------------------------------------------------------------

# Set your destination folder
BINPATH=$(cd `dirname $0`; pwd)
POSTPATH="${BINPATH}/all posts/${POST_CATEGORY}/_posts"
SRCPATH="${BINPATH}/src"
DRAFTPATH="${BINPATH}/_drafts"

if [[ "${1}" == "-c" || "${1}" == "--create" ]]; then
    DIST_FOLDER="$POSTPATH"
    POST_FILE_NAME="${CURRENT_DATE}-${POST_NAME}.md"
    DEST_THUMB_IMAGE="${CURRENT_DATE}-${POST_NAME}.${SRC_THUMB_IMAGE##*.}"
fi

if [[ "${1}" == "-d" || "${1}" == "--draft" ]]; then
    DIST_FOLDER="$DRAFTPATH"
    POST_FILE_NAME="${POST_NAME}.md"
    DEST_THUMB_IMAGE="${POST_NAME}.${SRC_THUMB_IMAGE##*.}"
fi

if [[ "${1}" == "-p" || "${1}" == "--publish" ]]; then
    DIST_FOLDER="$POSTPATH"
    POST_FILE_NAME="${CURRENT_DATE}-${POST_NAME}.md"
    IMG_EXT=$(ls --ignore=*md "$DRAFTPATH" | grep "${POST_NAME}")
    IMG_EXT=${IMG_EXT##*.}
    DEST_THUMB_IMAGE="${CURRENT_DATE}-${POST_NAME}.${IMG_EXT}"
fi


# ----------------------------------------------------------------



# ------------------------------------------------------------------------------
# | UTILS                                                                      |
# ------------------------------------------------------------------------------

# Header logging
e_header() {
    printf "$(tput setaf 38)→ %s$(tput sgr0)\n" "$@"
}

# Success logging
e_success() {
    printf "$(tput setaf 76)✔ %s$(tput sgr0)\n" "$@"
}

# Error logging
e_error() {
    printf "$(tput setaf 1)✖ %s$(tput sgr0)\n" "$@"
}

# Warning logging
e_warning() {
    printf "$(tput setaf 3)! %s$(tput sgr0)\n" "$@"
}



# ------------------------------------------------------------------------------
# | MAIN FUNCTIONS                                                             |
# ------------------------------------------------------------------------------

# Everybody need some help
initpost_help() {

cat <<EOT
------------------------------------------------------------------------------
INIT POST - A shortcut to create an initial structure for my posts.
------------------------------------------------------------------------------
Usage: ./initpost.sh [options] <post title> <post image path> <type> <size> <category>
Options:
  -h, --help        output instructions
  -c, --create      create post
  -d, --draft       create draft post
  -p, --publish     publish/promote a draft to a post
Arguments:
  <post title> : title of the post with quotes if it has spaces
  <post image path> : absolute local image path
  <type> : choose from {post, project, moment}
  <size> : choose from {1x1, 1x2, 2x2}
  <category> : post category
Example:
  ./initpost.sh -c "How to replace strings with sed" "/home/user/new-post.png" "post" "2x2"
Important Notes:
  - Don't forget the quotes
  - This script was created to generate new text files to my blog.
Copyright (c) Vitor Britto
Licensed under the MIT license.
------------------------------------------------------------------------------
EOT

}

# Initial Content
initpost_content() {

echo "---"
echo "layout: post"
echo "title: \"${POST_TITLE}\""
echo "date: \"${CURRENT_DATE} ${TIME}\""
echo "image: '/assets/img/posts/thumbnails/${DEST_THUMB_IMAGE}'"
echo "description:" 
echo "tags: "
echo "- first tag"
echo "twitter_text: \"${POST_TITLE}\""
echo "permalink: /articles/:title/"
echo "block: \"${POST_SIZE}\""
echo "type: ${POST_TYPE}"
echo "---"

}

# Create category layout
new_category_layout(){
    lower_cat=$(echo "$POST_CATEGORY" | awk '{print tolower($0)}')
    echo "---"
    echo "layout: category"
    echo "title: $POST_CATEGORY"
    echo "category: $POST_CATEGORY"
    echo "pagination: "
    echo "  enabled: true"
    echo "  category: android"
    echo "---"
}

# Check for category
category_check(){
    e_header "Category check"
    slug_cat=$(echo "$POST_CATEGORY" | iconv -t ascii//TRANSLIT | sed -r s/[^a-zA-Z0-9]+/-/g | sed -r s/^-+\|-+$//g | tr A-Z a-z)
    if [ ! -f category/$slug_cat.html ]; then
        e_warning "Category not found"
        e_header "  New category: $POST_CATEGORY"
        new_category_layout > "category/$slug_cat.html"
        mkdir -p "all posts/$POST_CATEGORY/_posts"
        e_success "Category was successfully created!"
    fi
}


# Create post
initpost_file() {
    if [ ! -f "$POST_FILE_NAME" ]; then
        e_header "Creating template..."
        category_check
        initpost_content > "${DIST_FOLDER}/${POST_FILE_NAME}"
        cp "$SRC_THUMB_IMAGE" "${SRCPATH}/img/posts/thumbnails/${DEST_THUMB_IMAGE}"
        e_success "Initial post successfully created!"
    else
        e_warning "File already exist."
        exit 1
    fi
}

# Create draft
initdraft_file() {
    if [ ! -f "$POST_FILE_NAME" ]; then
        e_header "Creating draft template..."
        initpost_content > "${DIST_FOLDER}/${POST_FILE_NAME}"
        echo -e "\nCategory=$POST_CATEGORY#Please don't remove this line" >>"${DIST_FOLDER}/${POST_FILE_NAME}"
        cp "$SRC_THUMB_IMAGE" "${DIST_FOLDER}/${DEST_THUMB_IMAGE}"
        e_success "Initial draft successfully created!"
    else
        e_warning "File already exist."
        exit 1
    fi

}

# Promote draft
promote_draft() {
    if [ ! -f "$POST_FILE_NAME" ]; then
        e_header "Promoting draft..."
        POST_CATEGORY=$(cat "${DRAFTPATH}/${POST_NAME}.md" | tail -n1 | grep -o -P '(?<=Category=).*(?=#Please)')
        POSTPATH="${BINPATH}/all posts/${POST_CATEGORY}/_posts"
        category_check
        head -n -1 "${DRAFTPATH}/${POST_NAME}.md" > temp.md ; mv temp.md "${DRAFTPATH}/${POST_NAME}.md"
        if 
            mv "${DRAFTPATH}/${POST_NAME}.md" "${POSTPATH}/${CURRENT_DATE}-${POST_NAME}.md"; 
            mv "${DRAFTPATH}/${POST_NAME}.${IMG_EXT}" "${SRCPATH}/img/posts/thumbnails/${DEST_THUMB_IMAGE}";
        then
            sed -i -e "s/date: .*/date: ${CURRENT_DATE} ${TIME}/" "${POSTPATH}/${CURRENT_DATE}-${POST_NAME}.md"
            e_success "Draft promoted successfully!"
        else
            e_warning "File already exists or draft promotion failed."
            exit 1
        fi
    fi
}

# ------------------------------------------------------------------------------
# | INITIALIZE PROGRAM                                                         |
# ------------------------------------------------------------------------------

main() {

    # Show help
    if [[ "${1}" == "-h" || "${1}" == "--help" ]]; then
        initpost_help ${1}
        exit
    fi

    # Create
    if [[ "${1}" == "-c" || "${1}" == "--create" ]]; then
        initpost_file $*
        exit
    fi

    # Draft
    if [[ "${1}" == "-d" || "${1}" == "--draft" ]]; then
        initdraft_file $*
        exit
    fi

    # Promote
    if [[ "${1}" == "-p" || "${1}" == "--promote" ]]; then
        promote_draft $*
        exit
    fi

}

# Initialize
main $*